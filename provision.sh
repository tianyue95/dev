export DEBIAN_FRONTEND=noninteractive
apt-get -qq update

### user
user=me
pass=developer
home=/home/$user
passfile=/home/vagrant/passfile
bashrc=/home/$user/.bashrc
bash_profile=/home/$user/.bash_profile
adduser -gecos "" --disabled-password $user
chpasswd <<<"${user}:${pass}"
usermod -aG sudo $user
echo ". .bashrc" >> $bash_profile
echo "#!/bin/sh"       > $passfile
echo "echo \"$pass\"" >> $passfile
chmod a+x $passfile
apt-get install -qq git
sudo -u $user git config --global user.name $user
sudo -u $user git config --global user.email ${user}@example.com

### shared folder
share=/home/${user}/dev
mkdir -p $share
mount --bind /vagrant $share
mount -o remount,uid=$(id -u $user),gid=$(id -u $user) $share

### auto login
service_d="/etc/systemd/system/getty@tty1.service.d"
override=${service_d}/override.conf
mkdir -p $service_d
echo "[Service]"    > $override
echo "Type=simple" >> $override
echo "ExecStart="  >> $override
echo "ExecStart=-/sbin/agetty --autologin $user --noclear %I 38400 linux" >> $override

### homebrew
apt-get install -qq wget curl
if ! command -v brew &> /dev/null
then
    wget -O install_brew.sh https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh
    chmod +x install_brew.sh
    sudo -u $user bash -c "SUDO_ASKPASS=$passfile NONINTERACTIVE=1 ./install_brew.sh"
    rm install_brew.sh
    brew=/home/linuxbrew/.linuxbrew/bin/brew
    echo "eval \"\$($brew shellenv)\"" >> $bashrc
fi

### emacs
if ! command -v emacs &> /dev/null
then
    apt-get install -qq git build-essential autoconf libgtk-3-dev libwebkit2gtk-4.0-dev
    apt-get build-dep -qq emacs
    git clone --shallow-since 2022-12-01 --branch emacs-29 https://git.savannah.gnu.org/git/emacs.git
    cd emacs
    git checkout 0a61e4e2b7189679df8ab3617e174b8b36afcf80
    ./autogen.sh
    ./configure --with-cairo --with-xwidgets --with-x-toolkit=gtk3
    make
    make install
    cd .. && rm -r emacs
fi

### hack ligature font
fonts_dir=/home/${user}/.local/share/fonts
sudo -u $user mkdir -p ${fonts_dir}
git clone https://github.com/pyrho/hack-font-ligature-nerd-font.git
cp hack-font-ligature-nerd-font/font/* ${fonts_dir}
rm -r hack-font-ligature-nerd-font
chown -R $user ${fonts_dir}
echo "fc-cache -f"
sudo -u $user fc-cache -f

### kimacs
config=/home/${user}/.config
kimacs=${config}/emacs
apt-get install -qq cmake libtool libtool-bin ripgrep fonts-noto-color-emoji
sudo -u $user $brew install git-delta tuntox
sudo -u $user mkdir -p $config
git clone --branch vagrant https://gitlab.com/kimok/kimacs.git $kimacs
chown -R $user $kimacs
chmod -R a+rw $kimacs
sudo -u $user bash $kimacs/install.sh
wait $!
echo "alias kimacs=emacs" >> $bashrc
echo "alias kmcs=emacs"   >> $bashrc

### exwm
apt-get install -qq xorg x11-xserver-utils
sudo -u $user tee -a /home/${user}/.xinitrc <<EOF > /dev/null
[ -f /etc/xprofile ] && . /etc/xprofile
[ -f ~/.xprofile ] && . ~/.xprofile
exec emacs --exwm
EOF
sudo -u $user tee -a /home/${user}/.xprofile <<EOF > /dev/null
xset r rate 180 40
xrandr --newmode "1920x1080_60.00" 173.00 1920 2048 2248 2576 1080 1083 1088 1120 -hsync +vsync
xrandr --addmode Virtual1 1920x1080_60.00

xrandr --newmode "1000x1000_60.00" 83.00 1000 1064 1168 1336 1000 1003 1013 1038 -hsync +vsync
xrandr --addmode Virtual1 1000x1000_60.00

xrandr --newmode "2560x1440_60.00"  312.25  2560 2752 3024 3488  1440 1443 1448 1493 -hsync +vsync
xrandr --addmode Virtual1 2560x1440_60.00

xrandr -s 1000x1000_60.00
EOF
sudo -u $user tee -a $bash_profile <<EOF > /dev/null
if [ -z "\${DISPLAY}" ] && [ "\${XDG_VTNR}" -eq 1 ]; then
   exec startx
fi
EOF

### desktop programs
apt-get install -qq firefox-esr

### graalvm ###
apt-get install -qq wget
url=https://github.com/graalvm/graalvm-ce-builds/releases/download/
version=22.3.0
prefix=graalvm-ce-java19
arch=linux-amd64
archive=${prefix}-${arch}-${version}.tar.gz
target=/usr/local/share
graal_home=${target}/graalvm
rm -rf $graal_home
wget -q ${url}vm-${version}/${archive}
mkdir -p $target
tar -xzf $archive
mv ${prefix}-${version} $graal_home
rm $archive
tee -a $bashrc <<EOF 
export PATH=${graal_home}/bin:\$PATH
export JAVA_HOME=$graal_home
export GRAALVM_HOME=$graal_home
EOF

### llvm on graalvm ###
${graal_home}/bin/gu install llvm llvm-toolchain
echo "export LLVM_TOOLCHAIN=\$(\$JAVA_HOME/bin/lli --print-toolchain-path)" >> $bashrc

### clojure ###
apt-get install -qu curl
sudo -u $user $brew install clojure/tools/clojure
version="2022.12.09-15.51.10"
target=/usr/local/bin/clojure-lsp
curl -o $target https://github.com/clojure-lsp/clojure-lsp/releases/download/${version}/clojure-lsp
chmod +x $target
sudo -u $user brew install leiningen

### javascript ###
apt-get install -qq unzip curl
sudo -u $user bash -c "curl -fsSL https://bun.sh/install | bash"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | sudo -u $user bash
export NVM_DIR="/home/${user}/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
nvm install --lts

### bash ###
npm i -g bash-language-server

### cleanup
rm $passfile
